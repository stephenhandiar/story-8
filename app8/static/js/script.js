$(function(){

     // jQuery methods go here...
      $("#myInput").on("keyup", function(e) {
           q = e.currentTarget.value.toLowerCase()
           console.log(q)
 
         $.ajax({
             url: "data/?q=" + q,
             datatype: 'json',
             
             success: function(data){
             $('tbody').html('')
             var result;
             
             for(var i = 0; i < data.items.length; i++) {
                 
                 result += "<tr>" +
                 "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                 "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                 "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                 "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                 "<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td></tr>";
             }
             $('tbody').append(result);
             console.log("testDone");
             }
         });
     });
 });
 