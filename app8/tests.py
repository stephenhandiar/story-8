from django.test import TestCase
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *

# Create your tests here.

class LibraryUnitTest(TestCase):

    def test_library_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_library_url_doesnt_exist(self):
        response = Client().get('/cari-cari')
        self.assertEqual(response.status_code, 404)

    def test_data_url_is_exist(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

    def test_library_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
