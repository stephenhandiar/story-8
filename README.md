[![pipeline status](https://gitlab.com/stephenhandiar/story-8/badges/master/pipeline.svg)](https://gitlab.com/stephenhandiar/story-8/commits/master)

[![coverage report](https://gitlab.com/stephenhandiar/story-8/badges/master/coverage.svg)](https://gitlab.com/stephenhandiar/story-8/commits/master)

## URL

[http://storydepelen.herokuapp.com/](http://storydepelen.herokuapp.com/)

## Authors

* **Stephen Handiar Christian**